# -*- mode: ruby -*-
# vi: set ft=ruby :

# Lire la clé publique à partir du fichier
ssh_key = File.read("#{ENV['VAGRANT_CWD']}/vagrant_key_ecdsa.pub").strip

VAGRANT_API_VERSION = "2"

# Configuration des machines
MACHINES = {
  'debian10-test' => {
    'box' => 'debian/buster64',
    'cpus' => 1,
    'memory' => 1024,
    'forwarded_port' => 2222,
    'ip' =>  '192.168.56.178'
  },
  'debian11-test' => {
    'box' => 'debian/bullseye64',
    'cpus' => 1,
    'memory' => 1024,
    'forwarded_port' => 2223,
    'ip' =>  '192.168.56.179'
  },
  'centos7-test' => {
    'box' => 'centos/7',
    'cpus' => 1,
    'memory' => 1024,
    'forwarded_port' => 2224,
    'ip' =>  '192.168.56.180'
  },
  'alma8-test' => {
    'box' => 'almalinux/8',
    'cpus' => 1,
    'memory' => 1024,
    'forwarded_port' => 2225,
    'ip' =>  '192.168.56.177'
  },
  'debian12-test' => {
    'box' => 'debian/bookworm64',
    'cpus' => 1,
    'memory' => 1024,
    'forwarded_port' => 2227,
    'ip' =>  '192.168.56.182'
  },
}

Vagrant.configure(VAGRANT_API_VERSION) do |config|
  # Désactiver le montage du dossier courant
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Increase boot timeout to prevent connection issues
  config.vm.boot_timeout = 600

  # Activer vagrant-hostmanager
  config.vbguest.auto_update = true
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = true
  
  config.vbguest.auto_update = false

  MACHINES.each do |box_name, box_config|
    config.vm.define box_name do |box|
      box.vm.box = box_config['box']
      box.vm.hostname = box_name
      pkg = if box_name.include?("debian") || box_name.include?("ubuntu")
        "apt"
      elsif box_name.include?("centos") || box_name.include?("fedora") || box_name.include?("redhat")
        "yum"
      else
        "unknown"
      end

      # Configuration du réseau
      box.vm.network "private_network", ip: box_config['ip']
      box.vm.network "forwarded_port", guest: 22, host: box_config['forwarded_port']
      if box_name == 'debian10-test'
        box.vm.network "forwarded_port", guest: 443, host: 4443
        box.vm.network "forwarded_port", guest: 8443, host: 8443
        box.vm.network "forwarded_port", guest: 80, host: 8000
        box.vm.network "forwarded_port", guest: 10051, host: 10051
        box.vm.disk :disk, name: "data", size: "20GB"
      end

      # Configuration des ressources CPU et mémoire
      box.vm.provider "virtualbox" do |vb|
        vb.memory = box_config['memory']
        vb.cpus = box_config['cpus']
      end

      # Ajouter la clé publique aux authorized_keys
      box.vm.provision "shell", inline: <<-SHELL
        echo "#{ssh_key}" >> /home/vagrant/.ssh/authorized_keys
        chmod 600 /home/vagrant/.ssh/authorized_keys
        "#{pkg}" update -y
        "#{pkg}" install vim curl -y
      SHELL
    end
  end
end
