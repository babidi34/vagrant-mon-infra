# README for Vagrantfile

## Introduction

This Vagrantfile defines multiple virtual machines (VMs) with different operating systems and configurations. It's designed for development and testing purposes.

## Prerequisites

Vagrant: https://www.vagrantup.com/downloads.html

VirtualBox (or another provider): https://www.virtualbox.org/

vagrant-hostmanager plugin:
```
vagrant plugin install vagrant-hostmanager
```
vagrant-virtualbox plugin:
```
vagrant plugin install vagrant-virtualbox
```

## WSL (Windows Subsystem for Linux)

To use Vagrant within WSL, follow these steps:

1. Install Vagrant inside your WSL Linux distribution. Download the installer from the [Vagrant releases page](https://www.vagrantup.com/downloads) and install it.
2. Enable Windows access by setting the following environment variable:

       $ export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"

3. Make sure VirtualBox (or your desired provider) is installed on Windows and add it to your PATH in WSL:

       export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"

This minimal setup will allow you to run Vagrant inside WSL, accessing VirtualBox or Hyper-V from Windows.

## Additional Information

Refer to the Vagrant documentation for more information on configuring and connecting to VMs: [https://www.vagrantup.com/docs/](https://www.vagrantup.com/docs/)

## SSH Key Generation

Open a terminal and run 
```
ssh-keygen -t ecdsa -f vagrant_key_ecdsa -N ''
```
Place the generated vagrant_key_ecdsa and vagrant_key_ecdsa.pub files in the same directory as the Vagrantfile.

## Creating a Specific Machine

Navigate to the directory containing the Vagrantfile.
Use the command 
```
vagrant up <machine_name>
```
replacing <machine_name> with the desired machine (e.g., vagrant up debian10-test).

## Forwarded Ports

SSH access is forwarded to different ports for each machine (e.g., port 2222 for debian10-test).
Additional ports are forwarded for specific machines (e.g., ports 80, 443, 8443, and 10051 for debian10-test).
To change the forwarded ports, modify the forwarded_port settings within the Vagrantfile or modify the box_name in if condition.

## Basic Vagrant commands

- **Start a machine:**
  ```bash
  vagrant up <machine_name>
  ```

- **Connect to a machine via SSH:**
  ```bash
  vagrant ssh <machine_name>
  ```

- **Stop a machine:**
  ```bash
  vagrant halt <machine_name>
  ```

- **Destroy a machine (delete its files):**
  ```bash
  vagrant destroy <machine_name>
  ```

- **Restart a machine:**
  ```bash
  vagrant reload <machine_name>
  ```

- **Show the status of all machines:**
  ```bash
  vagrant status
  ```

## Potential Improvements

Customization: Allow users to easily modify the machine configurations (e.g., CPU, memory, ports) through a configuration file or command-line options.

Provisioning: Consider using provisioning tools like Ansible or Chef for more complex setup and configuration of the VMs.

## Connection methods

The following table shows the connection methods for each VM defined in the Vagrantfile:

| Machine       | SSH Port | Connection Command                                                                                                                                      |
|---------------|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| debian10-test | 2222     | `ssh vagrant@localhost -p 2222 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null`                                                             |
| debian11-test | 2223     | `ssh vagrant@localhost -p 2223 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null`                                                             |
| centos7-test  | 2224     | `ssh vagrant@localhost -p 2224 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null`                                                             |
| alma8-test    | 2225     | `ssh vagrant@localhost -p 2225 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null`                                                             |
| debian12-test | 2227     | `ssh vagrant@localhost -p 2227 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null`                                                             |


## SSH Key Location

The generated SSH key (vagrant_key_ecdsa) must be placed in the same directory as the Vagrantfile.

## Notes

Make sure to replace localhost with the IP address of your host machine if you're accessing the VMs from another machine.

The -o StrictHostKeyChecking=no and -o UserKnownHostsFile=/dev/null options are used to ignore security warnings and are not recommended for production use.

## Additional Information

Refer to the Vagrant documentation for more information on configuring and connecting to VMs: https://www.vagrantup.com/docs/
